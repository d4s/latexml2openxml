package com.epam.latexml2openxml.converters.image;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

import javax.imageio.ImageIO;

public class ImageConverter {
    public static boolean ConvertImageFile(String input, String output,
            ImageFormat targetFormat) {
        FileInputStream inputFile = null;
        FileOutputStream outputFile = null;
        try {
            inputFile = new FileInputStream(input);
            outputFile = new FileOutputStream(output);
            String formatName;
            switch (targetFormat) {
                case PNG:
                    formatName = "png";
                    break;
                default:
                    inputFile.close();
                    outputFile.close();
                    return false;
            }
            ImageIO.write(ImageIO.read(inputFile), formatName, outputFile);
        } catch (IOException e) {
            System.err.println("Error converting image:");
            e.printStackTrace(System.err);
            return false;
        } finally {
            if (inputFile != null)
                try {
                    inputFile.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            if (outputFile != null)
                try {
                    outputFile.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
        }
        return true;
    }
}
