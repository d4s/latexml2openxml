package com.epam.latexml2openxml.converters.presentation;

import java.awt.Color;

public class PresentationConverterUtils {
    public static Color parseColor(String nm ) throws NumberFormatException {
        if ( nm.startsWith("#") ) {
          nm = nm.substring(1);
        }
        nm = nm.toLowerCase();
        if (nm.length() > 6) {
          throw new NumberFormatException("nm is not a 24 bit representation of the color, string too long"); 
        }
        System.out.println("nm=" + nm );
        Color color = new Color( Integer.parseInt( nm , 16 ) );
        return color;
      }
}
