package com.epam.latexml2openxml.tokenizer;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.epam.latexml2openxml.tokenizer.tokens.DocumentToken;
import com.epam.latexml2openxml.tokenizer.tokens.Token;

public class Tokenizer implements ITokenizer {
    
    @Override
    public DocumentToken Tokenize(Document document) {
        if (document == null) {
            return null;
        }
        Element documentNode = document.getDocumentElement();
        Token documentToken = NodeTokenizer.Tokenize(documentNode);
        if (documentToken instanceof DocumentToken) {
            return (DocumentToken) documentToken;
        }
        return null;
    }
    
}
