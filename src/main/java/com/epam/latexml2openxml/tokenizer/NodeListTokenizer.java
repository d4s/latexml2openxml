package com.epam.latexml2openxml.tokenizer;

import java.util.ArrayList;
import java.util.List;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.epam.latexml2openxml.tokenizer.tokens.ModeToken;
import com.epam.latexml2openxml.tokenizer.tokens.OnlyToken;
import com.epam.latexml2openxml.tokenizer.tokens.Token;

public class NodeListTokenizer {
    public static List<Token> Tokenize(NodeList nodeList) {
        if (nodeList == null || nodeList.getLength() == 0) {
            return new ArrayList<Token>(0);
        }
        
        List<Token> tokenList = new ArrayList<Token>(nodeList.getLength());
        for (int i = 0; i < nodeList.getLength(); i++) {
            Node node = nodeList.item(i);
            if (node == null) {
                continue;
            }
            Token token = NodeTokenizer.Tokenize(node);
            if (token != null) {
                if (token instanceof ModeToken || token instanceof OnlyToken) {
                    tokenList.addAll(token.ChildTokens);
                } else {
                    tokenList.add(token);
                }
            }
        }
        return tokenList;
    }
}
