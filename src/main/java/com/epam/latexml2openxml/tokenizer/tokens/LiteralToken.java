package com.epam.latexml2openxml.tokenizer.tokens;

import org.w3c.dom.Node;

public class LiteralToken extends Token {
    public static final String NodeName = "#literal";
    
    public static Token Tokenize(Node node) {
        if (node == null || node.getNodeType() != Node.TEXT_NODE) {
            return null;
        }
        LiteralToken token = new LiteralToken();
        token.Text = node.getTextContent();
        if (token.Text != null) {
            if (token.Text.matches("^(\\\\n|\\s)*$")) {
                return null;
            }
            token.Text = token.Text.trim()
                    .replaceAll("\\n", "\n")
                    .replaceAll("\n$", "")
                    .replaceAll("\\s+", " ");
            return token;
        }
        return null;
    }
    
    public String Text;
    
    public LiteralToken() {
        Name = NodeName;
    }
    
}
