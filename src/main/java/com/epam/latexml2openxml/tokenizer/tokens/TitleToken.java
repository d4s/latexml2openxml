package com.epam.latexml2openxml.tokenizer.tokens;

import org.w3c.dom.Node;

public class TitleToken extends Token {
    
    public static final String NodeName = "title";
    
    public static Token Tokenize(Node node) {
        return Token.Tokenize(node, TitleToken.class, TitleToken.NodeName);
    };
    
    public TitleToken() {
        Name = NodeName;
    }
    
}
