package com.epam.latexml2openxml.tokenizer.tokens;

import org.w3c.dom.Node;

public class UnknownToken extends Token {
    public static Token Tokenize(Node node) {
        if (node == null) {
            return null;
        }
        UnknownToken token = new UnknownToken();
        token.Name = String.format("unknown#%s", node.getNodeName());
        token.Value = node.getNodeValue();
        token.TokenizeChildNodes(node);
        return token;
    }
    
    public String Value;
    
}
