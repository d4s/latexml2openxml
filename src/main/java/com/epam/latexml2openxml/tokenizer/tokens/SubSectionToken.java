package com.epam.latexml2openxml.tokenizer.tokens;

import java.util.Iterator;

import org.w3c.dom.Node;

public class SubSectionToken extends Token {
    public final static String NodeName = "subsection";
    
    public static Token Tokenize(Node node) {
        SubSectionToken token = Tokenize(node, SubSectionToken.class,
                SubSectionToken.NodeName);
        if (token != null && token.ChildTokens != null) {
            for (Iterator<Token> iterator = token.ChildTokens.iterator(); iterator
                    .hasNext();) {
                Token childToken = iterator.next();
                
                if (childToken instanceof TitleToken) {
                    token.Title = (TitleToken) childToken;
                    iterator.remove();
                }
            }
        }
        
        return token;
    }
    
    public TitleToken Title;
    
    public SubSectionToken() {
        Name = NodeName;
    };
}
