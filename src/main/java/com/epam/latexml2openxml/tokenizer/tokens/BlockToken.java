package com.epam.latexml2openxml.tokenizer.tokens;

import org.w3c.dom.Node;

public class BlockToken extends Token {
    public static final String NodeName = "block";
    
    public static Token Tokenize(Node node) {
        return Token.Tokenize(node, BlockToken.class, BlockToken.NodeName);
    };
    
    public BlockToken() {
        Name = NodeName;
    }
}
