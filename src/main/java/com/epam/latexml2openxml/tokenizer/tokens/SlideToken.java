package com.epam.latexml2openxml.tokenizer.tokens;

import java.util.Iterator;

import org.w3c.dom.Node;

public class SlideToken extends Token {
    public final static String NodeName = "slide";
    
    public static Token Tokenize(Node node) {
        SlideToken token = Token.Tokenize(node, SlideToken.class, SlideToken.NodeName);
        if (token!= null && token.ChildTokens != null)
        {
            for (Iterator<Token> iterator = token.ChildTokens.iterator(); iterator
                    .hasNext();) {
                Token childToken = iterator.next();
                if (childToken instanceof TitlePageToken)
                {
                    token.IsTitle = true;
                    iterator.remove();
                }
                if (childToken instanceof FrameTitleToken)
                {
                    token.Title = (FrameTitleToken) childToken;
                    iterator.remove();
                }
            }
        }
        return token;
    }
    
    public SlideToken() {
        Name = NodeName;
    };
    
    public boolean IsTitle = false;
    public FrameTitleToken Title;
}
