package com.epam.latexml2openxml.tokenizer.tokens;

import org.w3c.dom.Node;

public class PauseToken extends Token {
    public static final String NodeName = "pause";
    
    public static Token Tokenize(Node node) {
        return Token.Tokenize(node, PauseToken.class, PauseToken.NodeName);
    };
    
    public PauseToken() {
        Name = NodeName;
    }
}
