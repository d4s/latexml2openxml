package com.epam.latexml2openxml.tokenizer.tokens;

import org.w3c.dom.Node;

public class GraphicsToken extends Token {
    public final static String NodeName = "graphics";
    
    public static Token Tokenize(Node node) {
        GraphicsToken token = Token
                .Tokenize(node, GraphicsToken.class, GraphicsToken.NodeName);
        if (token!=null)
        {
            if (token.Attributes.containsKey("graphic"))
                token.Source = token.Attributes.get("graphic");            
        }
        return token;
    }
    
    public GraphicsToken() {
        Name = NodeName;
    };
    
    public String Source; 
    
}
