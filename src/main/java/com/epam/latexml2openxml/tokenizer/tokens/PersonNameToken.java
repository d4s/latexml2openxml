package com.epam.latexml2openxml.tokenizer.tokens;

import org.w3c.dom.Node;

public class PersonNameToken extends Token {
    public static final String NodeName = "personname";
    
    public static Token Tokenize(Node node) {
        return Token.Tokenize(node, PersonNameToken.class,
                PersonNameToken.NodeName);
    };
    
    public PersonNameToken() {
        Name = NodeName;
    }
}
