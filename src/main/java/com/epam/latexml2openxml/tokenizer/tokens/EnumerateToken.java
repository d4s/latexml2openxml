package com.epam.latexml2openxml.tokenizer.tokens;

import org.w3c.dom.Node;

public class EnumerateToken extends Token {
    
    public final static String NodeName = "enumerate";
    
    public static Token Tokenize(Node node) {
        return Token.Tokenize(node, EnumerateToken.class,
                EnumerateToken.NodeName);
    }
    
    public EnumerateToken() {
        Name = NodeName;
    };
    
}
