package com.epam.latexml2openxml.tokenizer.tokens;

import org.w3c.dom.Node;

public class ChapterToken extends Token {
    public final static String NodeName = "chapter";
    
    public static Token Tokenize(Node node) {
        return Token.Tokenize(node, ChapterToken.class, ChapterToken.NodeName);
    }
    
    public ChapterToken() {
        Name = NodeName;
    };
}
