package com.epam.latexml2openxml.tokenizer.tokens;

import org.w3c.dom.Node;

public class ParaToken extends Token {
    public static final String NodeName = "para";
    
    public static Token Tokenize(Node node) {
        return Token.Tokenize(node, ParaToken.class,
                ParaToken.NodeName);
    }
    
    public ParaToken() {
        Name = NodeName;
    };
}
