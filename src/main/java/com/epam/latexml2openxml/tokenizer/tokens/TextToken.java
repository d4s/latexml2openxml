package com.epam.latexml2openxml.tokenizer.tokens;

import org.w3c.dom.Node;

public class TextToken extends Token {
    public static final String NodeName = "text";
    
    public static Token Tokenize(Node node) {
        TextToken token = Token.Tokenize(node, TextToken.class,
                TextToken.NodeName);
        token.Text = "";
        
        for (Token childToken : token.ChildTokens) {
            if (childToken instanceof LiteralToken)
                token.Text += ((LiteralToken) childToken).Text;
        }
        token.ChildTokens.clear();
        return token;
    }
    
    public TextToken() {
        Name = NodeName;
    };
    
    public String Text;
    
}
