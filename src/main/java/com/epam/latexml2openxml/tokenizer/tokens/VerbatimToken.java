package com.epam.latexml2openxml.tokenizer.tokens;

import org.w3c.dom.Node;

public class VerbatimToken extends Token {
public static final String NodeName = "verbatim";
    
    public static Token Tokenize(Node node) {
        if (node == null)
            return null;
        VerbatimToken token = new VerbatimToken();
        token.TokenizeAttributes(node);
        token.Text = node.getTextContent();        
        return token;
    };
    
    public VerbatimToken() {
        Name = NodeName;
    }
    
    public String Text;
}
