package com.epam.latexml2openxml.tokenizer.tokens;

import org.w3c.dom.Node;

public class ParagraphToken extends Token {
    public static final String NodeName = "p";
    
    public static Token Tokenize(Node node) {
        return Token.Tokenize(node, ParagraphToken.class, ParagraphToken.NodeName);
    }
    
    public ParagraphToken() {
        Name = NodeName;
    };
}
