package com.epam.latexml2openxml.tokenizer.tokens;

import org.w3c.dom.Node;

public class ItemizeToken extends Token {
    public final static String NodeName = "itemize";
    
    public static Token Tokenize(Node node) {
        return Token.Tokenize(node, ItemizeToken.class, ItemizeToken.NodeName);
    }
    
    public ItemizeToken() {
        Name = NodeName;
    };
    
}
