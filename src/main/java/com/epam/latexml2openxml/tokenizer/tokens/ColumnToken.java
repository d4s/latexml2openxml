package com.epam.latexml2openxml.tokenizer.tokens;

import org.w3c.dom.Node;

public class ColumnToken extends Token {
    public static final String NodeName = "column";
    
    public static Token Tokenize(Node node) {
        return Token.Tokenize(node, ColumnToken.class, ColumnToken.NodeName);
    };
    
    public ColumnToken() {
        Name = NodeName;
    }
}
