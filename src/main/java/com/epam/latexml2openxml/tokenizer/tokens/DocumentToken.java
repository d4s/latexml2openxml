package com.epam.latexml2openxml.tokenizer.tokens;

import java.util.Iterator;

import org.w3c.dom.Node;

public class DocumentToken extends Token {
    public final static String NodeName = "document";
    
    public static Token Tokenize(Node node) {
        DocumentToken token = Token.Tokenize(node, DocumentToken.class,
                DocumentToken.NodeName);
        
        if (token != null && token.ChildTokens != null) {
            for (Iterator<Token> iterator = token.ChildTokens.iterator(); iterator
                    .hasNext();) {
                Token childToken = iterator.next();
                
                if (childToken instanceof LiteralToken) {
                    iterator.remove();
                }
                if (childToken instanceof TitleToken) {
                    token.Title = (TitleToken) childToken;
                    iterator.remove();
                }
                if (childToken instanceof CreatorToken)
                {
                    token.Creator = (CreatorToken) childToken;
                    iterator.remove();
                }
            }
        }
        return token;
    }
    
    public TitleToken Title;
    public CreatorToken Creator;
    
    public DocumentToken() {
        Name = NodeName;
    }
    
}
